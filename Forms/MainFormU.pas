unit MainFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ComCtrls, ActnList, Menus, ExtCtrls, HideFiles, ShellApi,
  LCLType;

type

  { TMainFrm }

  TMainFrm = class(TForm)
    actAppActions: TActionList;
    actAddFileToList: TAction;
    actDeleteFileFromList: TAction;
    actAddDirectoryToList: TAction;
    actCleanList: TAction;
    actClose: TAction;
    actAbout: TAction;
    actUnmask: TAction;
    actSaveAs: TAction;
    actMask: TAction;
    actOpenMaskFile: TAction;
    btnProcess: TBitBtn;
    btnUnmask: TBitBtn;
    btnAdd: TToolButton;
    btnAddFolder: TToolButton;
    btnClear: TToolButton;
    btnDelete: TToolButton;
    btnOpenDestinationFile: TSpeedButton;
    btnMask: TBitBtn;
    btnSaveAs: TSpeedButton;
    btnUnmask1: TBitBtn;
    dlgDirectory: TSelectDirectoryDialog;
    dlgOpenFiles: TOpenDialog;
    grpMaskFile: TGroupBox;
    grpSaveAs: TGroupBox;
    grpSecretFiles: TGroupBox;
    ilAppImages: TImageList;
    lstSecretFiles: TListBox;
    mitAbout: TMenuItem;
    mitHelp: TMenuItem;
    mitSep: TMenuItem;
    mitProcess: TMenuItem;
    mitClean: TMenuItem;
    mitSep2: TMenuItem;
    mitAddDir: TMenuItem;
    mitSep1: TMenuItem;
    mitDelete: TMenuItem;
    mitAddFilesToList: TMenuItem;
    mitClose: TMenuItem;
    mitApplication: TMenuItem;
    itmSecretFiles: TMenuItem;
    mmAppMenu: TMainMenu;
    dlgSave: TSaveDialog;
    pnlLeft: TPanel;
    Panel2: TPanel;
    tlbAppToolbar: TToolBar;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    txtMaskFile: TEdit;
    txtSaveAs: TEdit;
    procedure actAboutExecute(Sender: TObject);
    procedure actAddDirectoryToListExecute(Sender: TObject);
    procedure actAddFileToListExecute(Sender: TObject);
    procedure actCleanListExecute(Sender: TObject);
    procedure actCloseExecute(Sender: TObject);
    procedure actDeleteFileFromListExecute(Sender: TObject);
    procedure actMaskExecute(Sender: TObject);
    procedure actUnmaskExecute(Sender: TObject);
    procedure actOpenMaskFileExecute(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnMaskClick(Sender: TObject);
    procedure btnProcessClick(Sender: TObject);
    procedure btnUnmaskClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lstSecretFilesClick(Sender: TObject);
    procedure lstSecretFilesDblClick(Sender: TObject);
  private
    { private declarations }
    hfo: THideFilesObject;
  public
    { public declarations }
  end;

var
  MainFrm: TMainFrm;

implementation

{$R *.lfm}

{ TMainFrm }

procedure TMainFrm.FormCreate(Sender: TObject);
begin
  hfo := THideFilesObject.Create;
end;

procedure TMainFrm.FormDestroy(Sender: TObject);
begin
  hfo.Free;
end;

procedure TMainFrm.FormShow(Sender: TObject);
begin

end;

procedure TMainFrm.lstSecretFilesClick(Sender: TObject);
begin

end;

procedure TMainFrm.lstSecretFilesDblClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar(lstSecretFiles.Items[lstSecretFiles.ItemIndex]),
    nil, nil, SW_SHOWDEFAULT);
end;

procedure TMainFrm.actAddFileToListExecute(Sender: TObject);
var
  I: integer;
begin
  if dlgOpenFiles.Execute then
  begin
    for I := 0 to dlgOpenFiles.Files.Count - 1 do
    begin
      lstSecretFiles.Items.Add(dlgOpenFiles.Files[I]);
    end;
  end;
end;

procedure TMainFrm.actCleanListExecute(Sender: TObject);
begin
  lstSecretFiles.Clear;
  hfo.SecretFiles.Clear;
end;

procedure TMainFrm.actCloseExecute(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMainFrm.actAddDirectoryToListExecute(Sender: TObject);
var
  lst: TStringList;
  frm: TForm;
  pb: TProgressBar;
  I: integer;
begin
  if dlgDirectory.Execute then
  begin
    frm := TForm.Create(nil);
    frm.BorderStyle := bsToolWindow;
    frm.Height := 25;
    frm.Width := 200;
    frm.Position := poMainFormCenter;
    frm.Caption := 'Finding all files...please wait';
    frm.BorderIcons := frm.BorderIcons - [biSystemMenu];

    pb := TProgressBar.Create(frm);
    frm.InsertControl(pb);

    pb.Align := alClient;
    pb.Style := pbstMarquee;

    frm.FormStyle := fsStayOnTop;
    frm.Show;
    Application.ProcessMessages;

    lst := FindAllFiles(dlgDirectory.FileName, '*.*', True);

    Application.ProcessMessages;

    lstSecretFiles.Items.BeginUpdate;

    for I := 0 to lst.Count - 1 do
    begin
      lstSecretFiles.Items.Add(lst[I]);
      Application.ProcessMessages;
    end;

    lstSecretFiles.Items.EndUpdate;

    frm.Free;
    lst.Free;
  end;
end;

procedure TMainFrm.actAboutExecute(Sender: TObject);
begin
  ShowMessage
  (
    'HideMyFile' + LineEnding + '==================================================' +
    LineEnding + ' Utility for hiding multiple files under another file (jpeg image etc.)'
    + LineEnding + '==================================================' +
    LineEnding + 'Flakron Shkodra 2011'
    );
end;

procedure TMainFrm.actDeleteFileFromListExecute(Sender: TObject);
begin
  if lstSecretFiles.ItemIndex > -1 then
    try
      lstSecretFiles.Items.Delete(lstSecretFiles.ItemIndex);
    except
    end;
end;

procedure TMainFrm.actMaskExecute(Sender: TObject);
begin
  try
    hfo.SecretFiles.Clear;
    hfo.SecretFiles.AddStrings(lstSecretFiles.Items);
    hfo.MaskFile := txtMaskFile.Text;
    hfo.OutMaskedFile := txtSaveAs.Text;
    hfo.ExecuteHide;
  except
    on e: Exception do
      ShowMessage(e.Message);
  end;
end;

procedure TMainFrm.actOpenMaskFileExecute(Sender: TObject);
var
  dir: string;
begin

  if btnProcess.Caption = 'Mask' then
  begin
    dlgOpenFiles.Options := dlgOpenFiles.Options - [ofAllowMultiSelect];
    if dlgOpenFiles.Execute then
    begin
      txtMaskFile.Text := dlgOpenFiles.FileName;
    end;
    dlgOpenFiles.Options := dlgOpenFiles.Options + [ofAllowMultiSelect];
  end
  else
  begin
    if SelectDirectory(dir, [sdAllowCreate], 0) then
    begin
      txtMaskFile.Text := dir;
    end;
  end;

end;

procedure TMainFrm.actSaveAsExecute(Sender: TObject);
begin

  if btnProcess.Caption = 'Mask' then
  begin
    dlgSave.DefaultExt := ExtractFileExt(txtMaskFile.Text);
    dlgSave.Filter := '(' + dlgSave.DefaultExt + ')|*' + dlgSave.DefaultExt;
    if dlgSave.Execute then
    begin
      txtSaveAs.Text := dlgSave.FileName;
    end;
  end
  else
  begin
    if dlgOpenFiles.Execute then
    begin
      txtSaveAs.Text := dlgOpenFiles.FileName;
    end;
  end;

end;

procedure TMainFrm.actUnmaskExecute(Sender: TObject);
var
  I: integer;
begin

  try
    lstSecretFiles.Clear;
    hfo.OutputDir := txtMaskFile.Text;
    hfo.OpenFile(txtSaveAs.Text);
    for I := 0 to hfo.SecretFiles.Count - 1 do
    begin
      lstSecretFiles.Items.Add(hfo.SecretFiles[I]);
      Application.ProcessMessages;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.Message);
    end;
  end;

end;

procedure TMainFrm.btnClearClick(Sender: TObject);
begin
  lstSecretFiles.Clear;
end;

procedure TMainFrm.btnCloseClick(Sender: TObject);
begin
  Close();
end;

procedure TMainFrm.btnMaskClick(Sender: TObject);
begin
  lstSecretFiles.Clear;
  tlbAppToolbar.Visible := True;
  grpMaskFile.Caption := 'Mask File';
  grpSaveAs.Caption := 'Output File';
  btnProcess.Caption := 'Mask';
  btnProcess.Tag:=0;
  ilAppImages.GetBitmap(7, btnSaveAs.Glyph);
  btnSaveAs.Repaint;
  ilAppImages.GetBitmap(5, btnProcess.Glyph);
  btnProcess.Repaint;
  btnOpenDestinationFile.Repaint;
end;

procedure TMainFrm.btnProcessClick(Sender: TObject);
begin
  if btnProcess.Tag = 0 then
  begin
    actMask.Execute;
  end
  else
  begin
    actUnmask.Execute;
  end;
end;

procedure TMainFrm.btnUnmaskClick(Sender: TObject);
begin
  lstSecretFiles.Clear;
  tlbAppToolbar.Visible := False;
  grpMaskFile.Caption := 'Output Dir';
  grpSaveAs.Caption := 'Masked File';
  btnProcess.Caption := 'Unmask';
  btnProcess.Tag:=1;
  ilAppImages.GetBitmap(4, btnSaveAs.Glyph);
  btnSaveAs.Repaint;
  ilAppImages.GetBitmap(6, btnProcess.Glyph);
  btnProcess.Repaint;
  btnOpenDestinationFile.Repaint;
end;




end.

