unit HideFiles;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, zipper, ziptypes,LibTar;

type

  { THideFilesObject }

  THideFilesObject = class
  private
    _MaskFile: string;
    _OutMaskedFile: string;
    _OutputDir: string;
    _SecretFiles: TStringList;
    _SecretFolder: string;
  public
    constructor Create;
    property SecretFiles: TStringList read _SecretFiles write _SecretFiles;
    property MaskFile: string read _MaskFile write _MaskFile;
    property OutMaskedFile: string read _OutMaskedFile write _OutMaskedFile;
    property OutputDir: string read _OutputDir write _Outputdir;
    procedure ExecuteHide;
    procedure OpenFile(MaskedFile: string);
    destructor Destroy; override;
  end;

const
  ErrorNoFilesToHide = 'There are no files to hide';
  ErrorNoMaskFile =
    ' Mask file does not exist.It must be existing valid file format (Like image.jpg,etc).';
  ErrorUnableToArchiveFiles = 'Unable to archive secret files';

implementation

{ THideFilesObject }


constructor THideFilesObject.Create;
begin
  Randomize;
  _SecretFolder := GetTempDir + '\hmf' + IntToStr(Random(100));
  _SecretFiles := TStringList.Create;
end;

procedure THideFilesObject.ExecuteHide;
var
  zip: TZipper;
  I: integer;
  fsMask: TFileStream;
  fsOutMask: TFileStream;
  memArchive: TMemoryStream;
  a:Char;
  buff: PByte;
  bytesRead: LongInt;

begin

  if (SecretFiles = nil) then
    raise Exception.Create(ErrorNoFilesToHide);

  if (SecretFiles.Count = 0) then
    raise Exception.Create(ErrorNoFilesToHide);

  if not FileExists(MaskFile) then
    raise Exception.Create(ErrorNoMaskFile);

  //Create zip archive with secret files

  zip := TZipper.Create;
  try
    Randomize;
    zip.FileName := GetTempFilename(GetTempDir,'pz');

    for I := 0 to SecretFiles.Count - 1 do
    begin
      if FileExists(SecretFiles[I]) then
      begin
        zip.Entries.AddFileEntry(SecretFiles[I], ExtractFileName(SecretFiles[I]));
      end;
    end;

    //zip.BufferSize := 512;
    zip.ZipAllFiles;


    //append archive to OutMaskedFile

    if FileExists(zip.FileName) then
    begin
      try

        fsMask := TFileStream.Create(MaskFile, fmOpenRead or fmShareDenyNone);
        fsOutMask := TFileStream.Create(OutMaskedFile, fmCreate);

        memArchive := TMemoryStream.Create;
        memArchive.LoadFromFile(zip.FileName);

        fsOutMask.CopyFrom(fsMask, fsMask.Size);
        memArchive.Position:=0;
        GetMem(buff,512);
        while memArchive.Position<memArchive.Size do
        begin;
          bytesRead := memArchive.Read(buff^,512);
          fsOutMask.Write(buff^, bytesRead);
        end;
        FreeMem(buff,512);

      finally
        memArchive.Free;
        fsOutMask.Free;
        fsMask.Free;
      end;

      DeleteFile(zip.FileName);
    end
    else
    begin
      raise Exception.Create(ErrorUnableToArchiveFiles);
    end;

  finally
    zip.Free;
  end;

end;

procedure THideFilesObject.OpenFile(MaskedFile: string);
var
  unzip: TUnZipper;
  lst: TStringList;
  I: integer;
  fs: TMemoryStream;
  zipStream: TMemoryStream;
  pos: integer;
  tmpFileName: string;
  buff: PByte;
  readByte: byte;
  bytesRead:integer;
begin


  tmpFileName := GetTempDir+'zp'+IntToStr(Random(100))+'.zip';

  if not FileExists(MaskedFile) then
    raise Exception.Create(ErrorNoMaskFile);

  zipStream := TMemoryStream.Create;

  try
    fs := TMemoryStream.Create;
    fs.LoadFromFile(MaskedFile);
    fs.Position := 0;


    pos := 0;

    while fs.Position < fs.Size do
    begin
      try
        if fs.ReadByte=$50 then
          if fs.ReadByte = $4B then
            if fs.readByte = $03 then
              if fs.readByte = $04 then
              begin
                pos := fs.Position - 4;
                Break;
              end;



      except
      end;
    end;

    if pos > 0 then
    begin
      fs.Position := pos;
      GetMem(buff,512);
      while fs.Position<fs.Size do
      begin
        bytesRead := fs.Read(buff^,512);
        zipStream.Write(buff^,bytesRead);
      end;
      Freemem(buff,512);
      zipStream.SaveToFile(tmpFileName);
    end
    else
    begin
      raise Exception.Create('Could not unmask file!');
    end;

  finally
    fs.Free;
  end;

  SecretFiles.Clear;
  unzip := TUnZipper.Create;
  try
    unzip.FileName := tmpFileName;

    unzip.OutputPath := OutputDir;
    unzip.Examine;
    unzip.UnZipAllFiles;
    lst := FindAllFiles(OutputDir, '*.*', False);
    try
      for I := 0 to lst.Count - 1 do
      begin
        SecretFiles.Add(lst[I]);
      end;
    finally
      lst.Free;
    end;
  finally
    unzip.Free;
    zipStream.Free;
    DeleteFile(tmpFileName);
  end;
end;

destructor THideFilesObject.Destroy;
var
  lst: TStringList;
  I: integer;
begin
  lst := FindAllFiles(_SecretFolder, '*.*', False);
  try
    for I := 0 to lst.Count - 1 do
    begin
      DeleteFile(lst[I]);
    end;
  finally
    lst.Free;
  end;

  _SecretFiles.Free;

  inherited Destroy;
end;

end.

